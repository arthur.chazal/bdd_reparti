import com.mongodb.spark.MongoSpark
import org.apache.spark.sql._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.sql.functions.{array_contains, col, size}

import scala.collection.mutable.ArrayBuffer

case class node(var Pagerank : Double,var AdjNode : Array[String])


object Exercise1 extends App {
    val spark = SparkSession.builder()
      .master("local")
      .appName("MongoSparkConnectorIntro")
      .config("spark.mongodb.input.uri", "mongodb://127.0.0.1/test.spells")
      .config("spark.mongodb.output.uri", "mongodb://127.0.0.1/test.spells")
      .getOrCreate()
    spark.sparkContext.setLogLevel("WARN")
    val rdd = MongoSpark.load(sparkSession = spark)
    val rdd_filter = rdd.filter(array_contains(rdd("components"), " V") && size(col("components")) === 1 && col("level").rlike("(wizard [0-4])"))
    println("Nombre de sort : " + rdd_filter.count())
    rdd_filter.show()
    rdd_filter.foreach(ligne => println(ligne.toString()))
}
object Exercise2 extends App {
  var NoeudA = ("A", node(1.0, Array("B", "C")))
  var NoeudB = ("B", node(1.0, Array("C")))
  var NoeudC = ("C", node(1.0, Array("A")))
  var NoeudD = ("D", node(1.0, Array("C")))
  var Graph = Array(NoeudA, NoeudB, NoeudC, NoeudD)
  val conf = new SparkConf()
    .setAppName("Page Rank Graph")
    .setMaster("local[*]")
  val sc = SparkContext.getOrCreate(conf)
  sc.setLogLevel("ERROR")
  var RddGraph = sc.makeRDD(Graph)
  for (a <- 0 until 20)
  {
    val rddGraphFlat = RddGraph.flatMap(elem => {
      val results = new ArrayBuffer[(String, Double)]()
      elem._2.AdjNode.foreach(x =>
        results += Tuple2(x, (elem._2.Pagerank / elem._2.AdjNode.size))
      )
      if (elem._1 == "D")
        results += Tuple2("D", 0)
      results
    })
    val rdd3 = rddGraphFlat.reduceByKey((a, b) => a + b).mapValues(value => 0.15 + 0.85 * value)
    RddGraph = RddGraph.join(rdd3).map(elem => (elem._1, node(elem._2._2, elem._2._1.AdjNode)))
  }
  RddGraph.foreach(ligne => println("Page "+ligne._1+": Rank :"+ligne._2.Pagerank+" avec les pages adjacentes "+ligne._2.AdjNode.mkString(" et ")))
}
