#pip install pyspark
#pip install pandas

from pyspark.sql import SparkSession

spark = SparkSession \
    .builder \
    .appName("Test Spark SQL") \
    .getOrCreate()

sc = spark.sparkContext

#Le fichier json est cense etre dans le path suivant, le changer si besoin
path = "get_spell_test/all_spells.json"

df = spark.read.json(path)
df.createOrReplaceTempView("spells")

sqlDF = spark.sql(
    "SELECT name, summary FROM spells WHERE string(components)='[ V]' AND (level LIKE '%wizard 4%' OR level LIKE '%wizard 3%' OR level LIKE '%wizard 2%' OR level LIKE '%wizard 1%' OR level LIKE '%wizard 0%');"
)
sqlDF.show()
sqlDF.count()

#Pour enregistrer le resultats dans un autre json 'restri'
sqlDF.toPandas().to_json('ResTri.json', orient='records')
