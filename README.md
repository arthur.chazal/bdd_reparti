# BDD_REPARTI

## Intro
Voici le git de l'integralité du projet. Dans ce readme vous trouverez quelques infos sur comment faire fonctionner les éléments.
Pour toute information complémentaire sur le fonctionnement, on peut se référer au compte-rendu PDF

### 1) crawler/scrapper :
Le fichier est "get_spell_test\get_spell_test\spiders\crawling_data.py"
- Fonctionne avec Python 3 et la lib Scrappy : 
   *pip install scrapy*
- Pour lancer le scraper :  Se mettre dans le dossier get_spell_test puis : 
                            **scrapy crawl spell_crawler -O resultats.json**

### 2) BDD MONGODB SPARK
Le fichier est "Devoir1.scala", partie 'Exercise1'
Nous avons auparavant installé un serveur MongoDB sur notre ordinateur en local puis importer le fichier jswon précédant via l'outil mongoimport fourni par MongoDB afin de stocker les objets du JSON dans une collection

### 3) Requete SQL Spark
Le fichier est "SparkRequest.py"
Il requiert d'avoir Spark d'installé et le module Python PySpark.
Veillez à indiquer le bon path pour le json qui sert à peupler le dataFrame.

### 4) PageRank
Le fichier est "Devoir1.scala", partie 'Exercise2'
