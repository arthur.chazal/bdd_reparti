# coding: utf-8
# La ligne ci-dessus s'appelle un shebang, en Python elle sert à indiquer l'encodage de votre fichier. Elle doit toujours être en premier.

# Import des librairies dont nous aurons besoin
import scrapy

class Spider_Spell_Scrawler(scrapy.Spider):
    # Nom Spider
    name = "spell_scrawler"
    # URL_a_crawl
    url = "https://aonprd.com/Spells.aspx?Class=All"

    def start_requests(self):
        yield scrapy.Request(url=self.url, callback=self.parse_all_spell)

    def parse_all_spell(self, response):
        products = response.xpath('//*[@id="main"]/table//tr')
        for spell in products:
            yield scrapy.Request("https://aonprd.com/"+spell.xpath('td[1]//span//b//a/@href').get(), callback=self.parse_spell,
                                 meta={'item': {
                                     'name': spell.xpath('td[1]//span//b//a//text()').get().lstrip(),
                                     'summary': spell.xpath('td[1]//span/text()').get()[2:]
                                 }})

    def parse_spell(self, response):
        item = response.meta['item']
        spell = response.xpath(
            '//*[@id="main"]/table//tr//td[1]//span//text()').getall()
        item['level'] = spell[spell.index('Level')+1].lstrip()
        item['components'] = spell[spell.index('Components')+1].split(',')
        if('Spell Resistance' in spell):
            print(spell[spell.index('Spell Resistance')+1].lstrip())
            item['spell_resistance'] = True if 'yes' in spell[spell.index('Spell Resistance')+1] else False
        else:
            item['spell_resistance'] =False
        return item


pass
